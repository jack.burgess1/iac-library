# Datadog IAC Library

## Description

Proposed example of an IAC Library for managing and deploying Datadog resources using Terraform IAC.

Shipped as Dockerfile / docker-compose to ease testing & development,
may also be setup to run on-schedule through Cloud Provider Function-As-A-Service.

### Dockerfile

Pull the official Terraform image from Dockerhub
Build a local terraform:latest image (enables customization of image)

### docker-compose

Runs the terraform:latest image, passing the terraform command provided as argument

### Secrets

Secrets are managed through SymLinks
The secret.ini and the contents of ./secret/ are added to .gitignore to prevent any future accidental commit.

### docker run

    # docker run terraform apply

### TO DO

- Add simple shared-storage in docker-compose (inputs / outputs)
