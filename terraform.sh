#!/bin/sh

cd /data

source /data/secrets.ini

terraform init -no-color

terraform plan -no-color

printf "yes\n" | terraform apply -no-color
