FROM alpine:latest AS terraform
RUN wget -O terraform.zip -qST5 https://releases.hashicorp.com/terraform/${VERSION:-0.15.0}/terraform_${VERSION:-0.15.0}_${OS:-linux}_${ARCH:-amd64}.zip
RUN unzip -o terraform.zip -d /usr/local/bin/
RUN rm terraform.zip
RUN chmod 0777 /usr/local/bin/terraform
COPY ./data /data
COPY ./terraform.sh /terraform.sh
COPY ./secrets.ini /data/
RUN chmod 0777 /terraform.sh
ENTRYPOINT /bin/sh -c "/terraform.sh"
