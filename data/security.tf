# Create a new Datadog Security Rule
resource "datadog_security_monitoring_rule" "kiwi_attack" {
  name = "kiwi_attack"

  message = "The rule has triggered"
  enabled = true

  query {
    name            = "errors"
    query           = "status:error @log_message:Kiwi"
    aggregation     = "count"
  }

  case {
    status        = "high"
    condition     = "errors > 1"
    notifications = ["@datadog-demo-jack"]
  }

  options {
    evaluation_window   = 300
    keep_alive          = 600
    max_signal_duration = 900
  }

  tags = ["fruit:kiwi"]
}

# Create a new Datadog Security Rule
resource "datadog_security_monitoring_rule" "apple_attack" {
  name = "apple_attack"

  message = "The rule has triggered"
  enabled = true

  query {
    name            = "errors"
    query           = "status:error @log_message:Apple"
    aggregation     = "count"
  }

  case {
    status        = "high"
    condition     = "errors > 1"
    notifications = ["@datadog-demo-jack"]
  }

  options {
    evaluation_window   = 300
    keep_alive          = 600
    max_signal_duration = 900
  }

  tags = ["fruit:apple"]
}
